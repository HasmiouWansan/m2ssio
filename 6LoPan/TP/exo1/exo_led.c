#include "contiki.h" 
#include "dev/leds.h"
#include "dev/button-sensor.h"
#include <stdio.h>

PROCESS(exo_led_process, "Test button");
AUTOSTART_PROCESSES(&exo_led_process);

int activeRed;
PROCESS_THREAD(exo_led_process, ev, data){
	
	PROCESS_BEGIN();
	activeRed = 1;
	SENSORS_ACTIVATE(button_sensor);
	while(1) {
		PROCESS_WAIT_EVENT_UNTIL(ev == sensors_event &&
					 data == &button_sensor);
		//leds_toggle(LEDS_ALL);
		
		if (ev == sensors_event && data == &button_sensor){
			printf("Button pressed!");
			if(activeRed){
				leds_off(LEDS_GREEN);
				leds_on(LEDS_RED);
				activeRed=0;
			}else{
				activeRed=1;
				leds_on(LEDS_GREEN);
				leds_off(LEDS_RED);
			}
		}
	}
    PROCESS_END();
}

/*leds_on(LEDS_RED);
leds_off(LEDS_GREEN);
leds_toggle(LEDS_RED);
leds_off(LEDS_ALL);*/

