#include "contiki.h"
#include "dev/leds.h"
#include <stdio.h>
#include "dev/button-sensor.h"

static struct etimer et;
static int i=0; 

PROCESS(count_process, "Count process");
AUTOSTART_PROCESSES(&count_process);

int * decToBinary(int n){
	static int  r[3];
	int i;
	for(i=0;n>0;i++){
        r[i]=n%2;    
        n=n/2; 
    }
    return r;
}

void switchOn(int b, int g, int r){
    leds_off(LEDS_ALL);
    if(b){
		//Bleu
		leds_on(LEDS_BLUE);
	}
	
	if(g){
		//Vert
		leds_on(LEDS_GREEN);
	}
	
	if(r){
		//Rouge
		leds_on(LEDS_RED);
	}
}

static int reset(){
	return 0;
}

PROCESS_THREAD(count_process, ev, data)
{
	PROCESS_BEGIN();
	/* Delay 1 second */
	etimer_set(&et, CLOCK_SECOND);
	
	SENSORS_ACTIVATE(button_sensor);
	//i=0;
	
	while(1){
		PROCESS_WAIT_EVENT();
	     	if(etimer_expired(&et)){     
			 etimer_reset(&et); 
			 int *b = decToBinary(i);
			 printf("Counting => %i mod 8: %i Binary %i%i%i.\n",i,(i%8),b[2],b[1],b[0]);
			 switchOn(b[2],b[1],b[0]);
	
		   i++;
		}

		if(ev == sensors_event && data == &button_sensor){
			printf("Button pressed!\n");
			i = reset();
		
		}
   	}
	
	PROCESS_END();
}
