package com.soap.main;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

public class ClientStub {
	public static void main(String[] args) throws ServiceException, RemoteException{
		Hello service = new HelloServiceLocator().getHello();
		System.out.println(service.sayHello("Diallo"));
	}
}
