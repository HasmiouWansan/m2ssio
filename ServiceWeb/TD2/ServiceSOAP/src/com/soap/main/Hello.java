package com.soap.main;

import java.io.Serializable;

public  class Hello implements Serializable {
	public static String name = "Asmiou";
	@Override
	public String toString() {
		return "Hello [getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
	public String sayHello(String name) {
		return "Hello "+name;
	}
}
