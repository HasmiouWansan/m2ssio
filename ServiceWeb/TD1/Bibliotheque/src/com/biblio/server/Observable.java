package com.biblio.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import com.biblio.common.IObservateur;
import com.biblio.common.IObservable;

public class Observable extends UnicastRemoteObject implements IObservable{
	
	private ArrayList<IObservateur> observateurList;
	private int value = 0;
	
	/**
	 * Le constructeur o� l'on initialise le tableau
	 * @throws RemoteException
	 */
	public Observable() throws RemoteException{
		observateurList = new ArrayList<IObservateur>();
	}
	
	/**
	 * La souscription en mode concurrentielle
	 */
	@Override
	public synchronized void subscribe(IObservateur obs) throws RemoteException {
		observateurList.add(obs);
	}
	
	/**
	 * Desabonnement en mode concurentiel
	 */
	@Override
	public synchronized void unsubscribe(IObservateur obs) throws RemoteException {
		observateurList.remove(obs);
	}
	
	public void notif(int v) {
		this.value = v;
		
		//propager l'information vers tous les autres
		IObservateur observateur;
		
		for(int i=0; i<this.observateurList.size(); i++) {
			try {
				
				observateur = (IObservateur) observateurList.get(i);
				observateur.notif(v);
			
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
