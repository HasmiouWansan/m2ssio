package com.biblio.server;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

import com.biblio.common.IObservateur;

public class ObservableServer {
	public ObservableServer() {
		try {
			LocateRegistry.createRegistry(1099);
			IObservateur obs = new Observateur();
			Naming.rebind("ObservableService", obs);
			for(int i=0; i<=30; i++) {
				obs.notif(i*30);
				Thread.sleep(1000);
			}
			System.out.println("Server OK");

		}catch (Exception e) {
			System.out.println("Errors: "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static void main(String args[]){
		new ObservableServer();
	}
}
