package com.biblio.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import com.biblio.common.IBook;

public class Book extends UnicastRemoteObject implements IBook {
	public Book(Long isbn, String title, String author) throws RemoteException{
		//super();
		this.isbn = isbn;
		this.author = author;
		this.title=title;

	}

	private String title;
	private Long isbn;
	private String author;
	
	public String getTitle() throws RemoteException{
		return title;
	}
	
	public void setTitle(String title) throws RemoteException{
		this.title = title;
	}
	
	public long getIsbn() throws RemoteException{
		return isbn;
	}
	
	public void setIsbn(long isbn) throws RemoteException{
		this.isbn = isbn;
	}
	
	public String getAuthor() throws RemoteException{
		return author;
	}
	
	public void setAuthor(String author) throws RemoteException{
		this.author = author;
	}
}
