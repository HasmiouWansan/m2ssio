package com.biblio.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import com.biblio.common.IObservateur;

public class Observateur extends UnicastRemoteObject implements IObservateur {

	public Observateur() throws RemoteException {
		//super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void notif(int value) throws RemoteException {
		System.out.println("Nouvelle valeur "+ value);
		
	}

}
