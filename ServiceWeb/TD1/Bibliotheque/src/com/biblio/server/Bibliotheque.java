package com.biblio.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.biblio.common.IBibliotheque;
import com.biblio.common.IBook;

public class Bibliotheque extends UnicastRemoteObject implements IBibliotheque {
	
	public HashMap<Long, IBook> books;
	
	protected Bibliotheque() throws RemoteException {
		//super();
		this.books = new HashMap<Long, IBook>();
	}

	@Override
	public List<IBook> find(String key) throws RemoteException {
		ArrayList<IBook> result = new ArrayList<IBook>();
		for(Map.Entry<Long, IBook> b: books.entrySet()) {
			if(b.getValue().getTitle().equals(key) || b.getValue().getAuthor().equals(key)) {
				result.add(b.getValue());
			}
		}
		return result;
	}

	@Override
	public void remove(long isbn) throws RemoteException {
		try {
			if(this.books.containsKey(isbn)){
				this.books.remove(isbn);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public IBook addBook(long isbn, String title, String author) throws RemoteException {
		try {
			if(!this.books.containsKey(isbn)) {
				IBook book = new Book(isbn, title, author);
				this.books.put(isbn, book);
				return book;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return null;
		
	}

	
}

