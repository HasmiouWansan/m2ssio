package com.biblio.client;

import java.rmi.Naming;

import com.biblio.common.IObservable;
import com.biblio.common.IObservateur;
import com.biblio.server.Observateur;

public class ObservateurClient{
	public static void main(String args[]){ 
		try{
			IObservateur obs1 = new Observateur(); 
			IObservateur obs2 = new Observateur(); 
			IObservateur obs3 = new Observateur(); 
			
			IObservable sub = (IObservable) Naming.lookup("ObservableService");
			
			sub.subscribe(obs1);
			sub.subscribe(obs2); 
			sub.subscribe(obs3);
		}catch(Exception e){
			e.printStackTrace();
		} 
	}
}
