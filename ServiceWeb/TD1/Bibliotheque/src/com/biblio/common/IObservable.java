package com.biblio.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IObservable extends Remote {
	public void subscribe(IObservateur obs) throws RemoteException;
	public void unsubscribe(IObservateur obs) throws RemoteException;
	
}
