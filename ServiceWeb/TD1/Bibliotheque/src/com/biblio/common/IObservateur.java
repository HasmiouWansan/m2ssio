package com.biblio.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IObservateur extends Remote {
	public void notif(int value) throws RemoteException;
}
