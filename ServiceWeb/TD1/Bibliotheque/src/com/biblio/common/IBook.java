package com.biblio.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IBook extends Remote {
	public String getTitle() throws RemoteException;
	public void setTitle(String title) throws RemoteException;
	public long getIsbn() throws RemoteException;
	public void setIsbn(long isbn) throws RemoteException;
	public String getAuthor() throws RemoteException;
	public void setAuthor(String author) throws RemoteException;
}
