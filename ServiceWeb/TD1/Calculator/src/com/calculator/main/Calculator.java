package com.calculator.main;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import com.calculator.common.ICalculator;

public class Calculator extends UnicastRemoteObject implements ICalculator {
	protected Calculator() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	public long add(long a, long b) throws RemoteException {
		return a+b;
	}

	public long sub(long a, long b) throws RemoteException {
		return a-b;
	}

	public long div(long a, long b) throws RemoteException {
		return a/b;
	}

	public long mult(long a, long b) throws RemoteException {
		return a*b;
	}
	
}
