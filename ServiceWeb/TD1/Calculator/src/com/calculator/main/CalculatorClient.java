package com.calculator.main;

import java.rmi.Naming;

import com.calculator.common.ICalculator;

public class CalculatorClient {
	public static long a = 10;
	public static long b = 7;

	public static void main(String[] args) {
		try {
			ICalculator calculator = (ICalculator) Naming.lookup("rmi://localhost:1099/CalculatorServcice");
			System.out.printf("Calcul: %d + %d = %d \n", a, b, calculator.add(a, b));
			System.out.printf("Calcul: %d - %d = %d \n", a, b, calculator.sub(a, b));
			System.out.printf("Calcul: %d * %d = %d \n", a, b, calculator.mult(a, b));
			System.out.printf("Calcul: %d / %d = %d \n", a, b, calculator.div(a, b));
		} catch (Exception e) {
			System.out.println("Erreur: " + e);
		}
	}
}
