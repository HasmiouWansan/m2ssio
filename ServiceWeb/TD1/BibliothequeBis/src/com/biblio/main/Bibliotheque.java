package com.biblio.main;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.biblio.common.Book;
import com.biblio.common.IBibliotheque;

public class Bibliotheque extends UnicastRemoteObject implements IBibliotheque {
	protected Bibliotheque() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	public HashMap<Long, Book> books = new HashMap<Long, Book>();
	
	
	
	@Override
	public List<Book> find(String key) throws RemoteException {
		ArrayList<Book> result = new ArrayList<Book>();
		for(Map.Entry<Long, Book> b: books.entrySet()) {
			if(b.getValue().getTitle().equals(key) || b.getValue().getAuthor().equals(key)) {
				result.add(b.getValue());
			}
		}
		return result;
	}

	@Override
	public void remove(long isbn) throws RemoteException {

		if(this.books.containsKey(isbn)){
			this.books.remove(isbn);
		}
	}

	@Override
	public Book addBook(long isbn, String title, String author) throws RemoteException {
		Book book = new Book();
		book.setIsbn(isbn);
		book.setTitle(title);
		book.setAuthor(author);
		
		if(this.books.containsKey(isbn)) {
			return null;
		}else {
			this.books.put(isbn, book);
			return book;
		}
		
	}

	
}

