package com.biblio.main;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

public class BibliothequeServer {

	public BibliothequeServer() {
		try {
			LocateRegistry.createRegistry(1099);
			Bibliotheque biblio = new Bibliotheque();
			Naming.rebind("BibliothequeService", biblio);
			System.out.println("Server OK");
			//Naming.rebind("rmi://localhost:1099/BibliothequeService", biblio);

		}catch (Exception e) {
			System.out.println("Errors: "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static void main(String args[]){
		new BibliothequeServer();
	}

}
