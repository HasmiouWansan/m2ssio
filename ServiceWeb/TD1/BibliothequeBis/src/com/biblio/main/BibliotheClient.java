package com.biblio.main;

import java.rmi.Naming;
import java.util.List;

import com.biblio.common.Book;
import com.biblio.common.IBibliotheque;

public class BibliotheClient {
	public static void main(String[] args) {
		try {
			IBibliotheque bibliotheque = (IBibliotheque) Naming.lookup("BibliothequeService");
			
			System.out.println(bibliotheque.addBook(1, "Le Roi lion", "DC"));
			List<Book> result = bibliotheque.find("Le Roi lion");
			for(Book b : result) {
				System.out.println(b);
			}
		
		} catch (Exception e) {
			System.out.println("Erreur: "+e.getMessage());
			e.printStackTrace();
		}
	}
}
