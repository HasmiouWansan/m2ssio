package com.biblio.common;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;


public interface IBibliotheque  extends Remote{
	public Book addBook(long isbn, String title, String author) throws RemoteException;
	
	public List<Book> find(String key) throws RemoteException;
	
	public void remove(long isbn) throws RemoteException;

	
}
