package fr.buserver.main;
public class Book {
	public Book() {}
	
	public Book(Long isbn, String title, String author){
		this.isbn = isbn;
		this.author = author;
		this.title=title;

	}

	private String title;
	private Long isbn;
	private String author;
	
	public String getTitle(){
		return title;
	}
	
	public void setTitle(String title){
		this.title = title;
	}
	
	public long getIsbn(){
		return isbn;
	}
	
	public void setIsbn(long isbn){
		this.isbn = isbn;
	}
	
	public String getAuthor(){
		return author;
	}
	
	public void setAuthor(String author){
		this.author = author;
	}
}
