package fr.buserver.main;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Bibliotheque {
	
	public HashMap<Long, Book> books;
	
	public Bibliotheque(){
		//super();
		this.books = new HashMap<Long, Book>();
	}

	public Book[] find(String key){
		ArrayList<Book> result = new ArrayList<Book>();
		for(Map.Entry<Long, Book> b: books.entrySet()) {
			if(b.getValue().getTitle().equals(key) || b.getValue().getAuthor().equals(key)) {
				result.add(b.getValue());
			}
		}
		Book[] b = new Book[result.size()];
		for(int i=0; i<result.size(); i++) {
			b[i]= result.get(i);
		}
		//return result.toArray(new Book[result.size()]);
		return b;
	}

	public void remove(long isbn) {
		try {
			if(this.books.containsKey(isbn)){
				this.books.remove(isbn);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public Book addBook(long isbn, String title, String author){
		try {
			if(!this.books.containsKey(isbn)) {
				Book book = new Book(isbn, title, author);
				this.books.put(isbn, book);
				return book;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return null;
		
	}

	
}

