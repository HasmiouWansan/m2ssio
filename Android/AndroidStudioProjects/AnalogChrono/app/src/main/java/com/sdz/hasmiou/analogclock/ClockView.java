package com.sdz.hasmiou.analogclock;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import java.util.Calendar;

import androidx.annotation.ColorInt;

public class ClockView extends View {
    long elapsedTime;
        static class State
        {
            long cumulatedTime = 0; // cumulated time from the previous runs
            long startTime = -1; // startTime for the current run

            public long getElapsedTime() {
                return cumulatedTime + ((startTime > 0)?(System.nanoTime() - startTime):0);
            }
        }

    public void setElapsedTime(long elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    private int height, width = 0;
    private int padding = 50;
    private int fontSize = 0;
    private int aiguille, aiguilleHeure = 0;
    private int rayon = 0;
    private Paint paint;
    private boolean isInit;
    private int[] hours = {1,2,3,4,5,6,7,8,9,10,11,12};
    private Rect rect = new Rect();

    public ClockView(Context context) {
        super(context);
    }

    public ClockView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ClockView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void initClock() {
        height = getHeight();
        width = getWidth();
        fontSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 13,
                getResources().getDisplayMetrics());
        int min = Math.min(height, width);
        rayon = min / 2 - padding;
        aiguille = min / 20;
        aiguilleHeure = min / 7;
        paint = new Paint();
        isInit = true;
    }

    /**
     * On rédefinie la methode appelé lors de la création de la vue
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        if (!isInit) {
            initClock();
        }

        //Définition de la couleur d'arrière plan
        //canvas.drawColor(Color.WHITE);

        //On dessine le cercle
        //drawCircle(canvas);

        //On déssine le point central
        drawCenter(canvas);

        //On déssine les heures
        //printHours(canvas);

        //On déssigne les aiguille
        drawHands(canvas);

        //Provoque l'invalidation de la zone spécifiée lors
        // d'un cycle suivant de la boucle d'événement.
        postInvalidateDelayed(500);
        invalidate();
    }

    /**
     * On calcul la position de chaque aiguille sur le cadran
     * @param canvas
     * @param loc
     * @param isHour
     */
    private void drawHand(Canvas canvas, double loc, boolean isHour,int color) {
        paint.setStrokeWidth(15);
        paint.setColor(color);
        //Calcul de l'angle
        double angle = Math.PI * loc / 30 - Math.PI / 2;

        //On charche l'inclinaison de l'aiguille
        int handRadius = isHour ? rayon - aiguille - aiguilleHeure : rayon - aiguille;

        //On dessine l'aiguille sous forme d'une ligne startX, sartY
        canvas.drawLine(width / 2, height / 2,
                (float) (width / 2 + Math.cos(angle) * handRadius),
                (float) (height / 2 + Math.sin(angle) * handRadius),
                paint);
    }

    /**
     * On déssine les aiguille en fonction de leurs longueurs, heure ou minute
     * @param canvas
     */
    private void drawHands(Canvas canvas) {
        //On recupère une instance de la calandrier
        Calendar c = Calendar.getInstance();

        //On récupère l'heure
        float hour = c.get(Calendar.HOUR_OF_DAY);

        //On la convertie au format Anglais
        hour = hour > 12 ? hour - 12 : hour;

        int redColor = getResources().getColor(android.R.color.holo_red_dark);
        int blackColor = getResources().getColor(android.R.color.darker_gray);
        //On déssine toutes les aiguilles
        drawHand(canvas, (hour + c.get(Calendar.MINUTE) / 60) * 5f, true,blackColor);
        drawHand(canvas, c.get(Calendar.MINUTE), false, blackColor);
        drawHand(canvas, c.get(Calendar.SECOND), false, redColor);
    }

    /**
     * On dessine les chiffres horaires sur le cadran
     * @param canvas
     */
    private void printHours(Canvas canvas) {
        //On défini la dimension de la police
        paint.setTextSize(fontSize);

        //On boucle sur la liste d'heure pour affichier
        for (int hour : hours) {
            //On stocke l'heure en string
            String value = String.valueOf(hour);

            //Récupérez la zone de limite de texte et stockez-la dans les limites.
            paint.getTextBounds(value, 0, value.length(), rect);

            //Calcul l'angle
            double angle = Math.PI / 6 * (hour - 3);

            //Calcul les positions des chiffres sur le cadran
            int x = (int) (width / 2 + Math.cos(angle) * rayon - rect.width() / 2);
            int y = (int) (height / 2 + Math.sin(angle) * rayon + rect.height() / 2);

            canvas.drawText(value, x, y, paint);
        }
    }

    /**
     * Dessine le point central de l'horloge
     * @param canvas
     */
    private void drawCenter(Canvas canvas) {
        paint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(width / 2, height / 2, 12, paint);
    }

    /**
     * Déssine le cadran de l'horloge
     * @param canvas
     */
    private void drawCircle(Canvas canvas) {
        paint.reset();
        paint.setColor(getResources().getColor(android.R.color.black));
        paint.setStrokeWidth(5);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
        canvas.drawCircle(width / 2, height / 2, rayon + padding - 10, paint);
       // canvas.drawBitmap(BitmapFactory.decodeFile(getURLForResources(R.drawable.swissclock)),width/2, height/2, paint);
    }

    private void drawBitmap(Canvas canvas){

    }
}
