package com.sdz.hasmiou.chronometre;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sdz.hasmiou.chronometre.R;

public class MainActivity extends AppCompatActivity {

    private Button btnStart,btnStop, btnInit;
    State state;
    private TextView txtChrono;
    private Handler handler;
    private Thread timerThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.txtChrono = (TextView) findViewById(R.id.txtChrono);
        this.btnInit = (Button) findViewById(R.id.btnInit);
        this.btnStart = (Button) findViewById(R.id.btnStart);
        this.btnStop = (Button) findViewById(R.id.btnStop);

        this.handler = new Handler();
        this.state = new State();
    }

    static class State{
        long cumulatedTime =0;
        long startTime =-1;
        String readableTime;

        public long getElapsedTime(){
            //Log.w("ElapstedTime: "+cumulatedTime);
            return cumulatedTime+((startTime >0)?(System.nanoTime()-startTime):0);
        }

        public String getReadableTime() {
            return readableTime;
        }

        public long getCumulatedTime() {
            return cumulatedTime;
        }

        public void setCumulatedTime(long cumulatedTime) {
            this.cumulatedTime = cumulatedTime;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public void setReadableTime(String readableTime) {
            this.readableTime = readableTime;
        }
    }

    private String getReadableTime(Long nanos) {
        long nanotemp = nanos;
        long tempSec = nanos / (1000 * 1000 * 1000);
        long sec = tempSec % 60;
        long min = (tempSec / 60) % 60;
        long hour = (tempSec / (60 * 60)) % 24;
        long day = (tempSec / (24 * 60 * 60)) % 24;
        nanotemp = nanotemp - tempSec;
        String numberStr="0";
        if(nanotemp>0){
            numberStr = String.valueOf(nanotemp).substring(0, 3);
        }
        return String.format("%d:%d:%d", hour, min, sec) + ":" + numberStr;
    }

    public void startChrono(){
        this.btnStart.setEnabled(false);
        this.btnStop.setEnabled(true);
        this.state.setStartTime(System.nanoTime());
        updateChronoOnUiThreadMethod();
    }

    public void stopChrono(){
        this.btnStart.setEnabled(true);
        if(this.btnStop.isEnabled()){
            this.state.setCumulatedTime(this.state.getElapsedTime());
            this.state.setStartTime(-1);
            if(this.timerThread!=null) timerThread.interrupt();
            this.btnStop.setEnabled(false);
        }
    }

    public void start(View v){
        this.startChrono();
    }

    public void stop(View v){
        this.stopChrono();
    }

    public void init(View v){

        this.txtChrono.setText("00:00:00");
        this.state.setStartTime(-1);
        this.state.setCumulatedTime(0);
        //btnStop.setEnabled(true);
    }

    public String timeFormat(long time){
        return this.getReadableTime(time);
    }

    /**
     * Utilisation du OnUiThread
     */
    public void updateChronoOnUiThreadMethod() {
        timerThread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String chrono = timeFormat(state.getElapsedTime());
                                //Log.w("chronoo", chrono + "the time in nano: " + state.getElapsedTime());
                                state.setReadableTime(chrono);
                                txtChrono.setText(state.getReadableTime());
                            }
                        });
                        try {
                            Thread.sleep(500);

                        } catch (InterruptedException e) {
                            Log.w("Error: ", e);
                        }
                    }
                } catch (Exception e) {
                }
            }
        };
        timerThread.start();

    }


}
